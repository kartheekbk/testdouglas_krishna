describe('DouglasTest', () => {

 //To Verify if User is able to login successfully   
    it('LoginToDouglas', () => {
    
    cy.visit("https://www.douglas.de/de")
    cy.acceptCookie()
    cy.clickOnUserProfileIcon()
    cy.loginWithEmailAndPassword("kartheek90@gmail.com","Test@12345")
    cy.verifyIfUserIsLoggedIn()
    })

   
//To Verify if Login is working with invalid credentials

it('LoginToDouglasWithIncorrectDetails', () => {

    cy.visit("https://www.douglas.de/de")
    cy.acceptCookie()
    cy.clickOnUserProfileIcon()
    cy.loginWithEmailAndPassword("kartheek90@gmail.com","Test@145")
    cy.checkForInvalidLoginError()

    })

//TO Verify if User is able to Logout

    it('LogOutfromDouglas', () => {

    cy.visit("https://www.douglas.de/de")
    cy.acceptCookie()
    cy.loginToDouglas()
    //cy.wait(10000);
	cy.logOut()
    cy.checkIfUserIsLoggedOff()

    })
    
//To verify if Session is being maintained when website is entered again while logged in
    it('SessionManagement', () => {
        cy.clearCookies()
        cy.visit("https://www.douglas.de/de")
        cy.acceptCookie()
        cy.loginToDouglas()
        cy.visit("https://www.douglas.de")
        cy.verifyIfUserIsLoggedIn()

    })

// To verify if User is able to use Forgot Password Option

    it('CheckResetPasswordOption', () => {

        cy.visit("https://www.douglas.de/de")
        cy.acceptCookie()
        cy.clickOnUserProfileIcon()
        cy.clickOnForgotPassword()
        cy.enterEmailID("kartheek90@gmail.com")
        cy.checkIfMailisTriggered()

        })

})